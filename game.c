#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "turn.h"

void game(char **joueurs, int nbJoueurs)
{
    int score[nbJoueurs];
    for (int i = 0; i < nbJoueurs; i++) {
        score[i] = 0;
    }
    int joueur = -1;
    bool exit = false;
    do {
        joueur = (joueur + 1) % nbJoueurs;
        printf("--------------------------\n");
        printf("It's %s\n's turn !", joueurs[joueur]);
        score[joueur] += turn();
        exit = exit || score[joueur] >= 13;
    } while (!exit || joueur != nbJoueurs - 1);
    for (int i = 0; i < nbJoueurs; i++) {
        if (score[i] >= 13) {
            printf("%s won !\n", joueurs[i]);
        }
    }
}

char **new_game(int n)
{
    char** data;
    char name[50];
    char c;
    data = (char**)malloc(sizeof(char**)*n);
    for (int i = 0; i < n; i++) {
        while ((c = getchar()) != '\n' && c != EOF) { } // Empty buffer
        printf("Name of player number %i : ", i);
        scanf("%49s", name);
        data[i] = (char*)malloc(sizeof(char) * (strlen(name) + 1));
        strcpy(data[i], name);
    }
    return data;
}

void delete_game(char **joueurs, int n)
{
    for (int i = 0; i < n; i++) {
        free(joueurs[i]);
    }
    free(joueurs);
}
