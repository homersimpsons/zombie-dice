CC=gcc
CFLAGS=-std=c99 -W -Wall
LDFLAGS=
EXEC=zombie

all: $(EXEC)

zombie: turn.o game.o main.o
	$(CC) -o zombie turn.o game.o main.o $(LDFLAGS)

turn.o: turn.c turn.h
	$(CC) -o turn.o -c turn.c $(CFLAGS)

game.o: game.c game.h
	$(CC) -o game.o -c game.c $(CFLAGS)

main.o: main.c main.h
	$(CC) -o main.o -c main.c $(CFLAGS)

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)

