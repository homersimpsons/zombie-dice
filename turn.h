#ifndef TURN
#define TURN
#include <stdbool.h>

/**
    Get the result (brain, footprints, shotgun) from a dice
    @param nbBrain number of faces with brain on the dice
    @param nbFootprints number of faces with footprints on the dice
    @param nbShotgun number of faces with shotgun on the dice
    @return BRAIN|FOOTPRINTS|SHOTGUN
*/
int lancerDe(int nbBrain, int nbFootprints, int nbShotgun);
/**
    Get the result of launching a specific DICE
    @param de DICE_GREEN|DICE_YELLOW|DICE_RED
    @return BRAIN|FOOTPRINTS|SHOTGUN
*/
int lancer(int de);
/**
    Ask user if he want to score and return his answer
    @return score == true
*/
bool doScore();
/**
    Pull a dice from stock and return it
    @param stock Current stock of dices
    @return DICE_GREEN|DICE_YELLOW|DICE_RED
*/
int getDice(int stock[]);
/**
    Get a human readable string for a dice (instead of int)
    @param dice DICE_GREEN|DICE_YELLOW|DICE_RED
    @return Readable string
*/
char *getHumanReadableDice(int dice);
/**
    Turn loop
    @return Score acquired within the loop
*/
int turn();

#endif
