#ifndef MAIN
#define MAIN

/**
    Entry point of Zombie Dice
    @return Success of the program
*/
int main();

#endif
