Zombie Dice
===========

Inroduction
-----------

The goal is to develop a [Zombie Dice Game](https://en.wikipedia.org/wiki/Zombie_Dice) in C, using console interaction

Specifications
--------------

- Multiple players
- Player names

Development
-----------

- Download the project
- `make mrproper && make all && ./zombie` Compile & Run
