#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "game.h"

int main()
{
    srand(time(NULL));
    int n;
    do {
        printf("How many players (2+)? ");
        scanf("%i", &n);
    } while (n < 2);
    char **joueurs;
    joueurs = new_game(n);
    game(joueurs, n);
    delete_game(joueurs, n);
    return 0;
}
