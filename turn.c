#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define DICE_GREEN   0 // 1, 2, 3
#define DICE_YELLOW  1 // 2, 2, 2
#define DICE_RED     2 // 3, 2, 1
#define DICE_UNDEF  -1
#define BRAIN        0
#define SHOTGUN      1
#define FOOTPRINTS   2
#define STOCK_GREEN  6
#define STOCK_YELLOW 4
#define STOCK_RED    3

int lancerDe(int nbBrain, int nbFootprints, int nbShotgun)
{
    int r = rand() % (nbBrain + nbShotgun + nbFootprints); // %6
    if (r < nbBrain) return BRAIN;
    if (r < nbBrain + nbFootprints) return FOOTPRINTS;
    return SHOTGUN;
}
int lancer(int de)
{
    if (de == DICE_GREEN) return lancerDe(3, 2, 1);
    if (de == DICE_YELLOW) return lancerDe(2, 2, 2);
    if (de == DICE_RED) return lancerDe(1, 2, 3);
    return -1;
}
bool doScore()
{
    char s;
    do {
        printf("Score (y or n)? ");
        scanf(" %c", &s);
    } while (s != 'y' && s != 'n');
    return s == 'y';
}
int getDice(int stock[])
{
    int r = rand() % (stock[DICE_GREEN] + stock[DICE_YELLOW] + stock[DICE_RED]);
    if (r < stock[DICE_GREEN]) return DICE_GREEN;
    if (r < stock[DICE_YELLOW]) return DICE_YELLOW;
    return DICE_RED;
}
char *getHumanReadableDice(int dice) {
    if (dice == DICE_UNDEF) return "????";
    if (dice == DICE_GREEN) return "GREEN";
    if (dice == DICE_YELLOW) return "YELLOW";
    if (dice == DICE_RED) return "HARD";
    return "Unknown";
}
int turn()
{
    int stock[3];
    stock[DICE_GREEN] = STOCK_GREEN;
    stock[DICE_YELLOW] = STOCK_YELLOW;
    stock[DICE_RED] = STOCK_RED;
    int hand[] = {DICE_UNDEF, DICE_UNDEF, DICE_UNDEF};
    int win[] = {0, 0, 0}; // Earned dice (brains), usefull to refill
    int game[] = {0, 0, 0}; // BRAIN, SHOTGUN, FOOTPRINTS
    do {
        for (int i = 0; i < 3; i++) {
            if (hand[i] == DICE_UNDEF) { // We bick a dice (if we don't get one)
                if (stock[DICE_GREEN] + stock[DICE_YELLOW] + stock[DICE_RED] == 0) { // If there are no dice left they get refilled from brains
                    printf("REFILL STOCK!\n");
                    for (int j = 0; j < 3; j++) {
                        stock[j] = win[j];
                        win[j] = 0;
                    }
                }
                int dice = getDice(stock);
                stock[dice]--;
                hand[i] = dice;
            }
        }
        for (int i = 0; i < 3; i++) {
            int res = lancer(hand[i]); // On joue le de
            game[res]++;
            if (res != FOOTPRINTS) {
                if (res == BRAIN) {
                    win[hand[i]]++;
                }
                hand[i] = DICE_UNDEF;
            }
        }
        printf("Brain: %i, SHOTGUN: %i\n", game[BRAIN], game[SHOTGUN]);
        printf("Hand: %s - %s - %s\n",
            getHumanReadableDice(hand[0]),
            getHumanReadableDice(hand[1]),
            getHumanReadableDice(hand[2])
        );
    } while (game[SHOTGUN] < 3 && !doScore());
    if (game[SHOTGUN] < 3) {
        return game[BRAIN];
    }
    return 0;
 }
