#ifndef GAME
#define GAME

/**
    Contain the game loop
    @param joueurs Array of players names (get with new_game)
    @param nbJoueurs Number of players
*/
void game(char **joueurs, int nbJoueurs);
/**
    Ask for players names
    @param n Number of players
    @return Array of players names
*/
char **new_game(int n);
/**
    Free memory
    @param joueurs Array of players names (get with new_game)
    @param n Number of players

*/
void delete_game(char **joueurs, int n);

#endif
